DROP TABLE lien;
DROP TABLE dictionnaire;


CREATE TABLE dictionnaire(
  ID int PRIMARY KEY ,
  mot varchar2(255)
);


CREATE TABLE lien (
 motID1 int NOT NULL REFERENCES dictionnaire(ID),
 motID2 int NOT NULL REFERENCES dictionnaire(ID),
 taille int NOT NULL,
 valeur int NOT NULL,
 constraint PK_D PRIMARY KEY (motID1, motID2, taille)
);
